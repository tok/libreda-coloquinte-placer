#!/bin/bash

# SPDX-FileCopyrightText: 2022 Thomas Kramer
#
# SPDX-License-Identifier: CC0-1.0

# Import the core code of coloquinte into this repository.

set -e

SRC_REPO=https://codeberg.org/tok/Coloquinte_placement

DEST_DIR=$(mktemp -d)

function clean () {
    rm -rf $DEST_DIR
}

trap clean EXIT

git -C $DEST_DIR clone $SRC_REPO coloquinte


mkdir -p src/coloquinte_cpp

cp -rv $DEST_DIR/coloquinte/src src/coloquinte_cpp/
cp -rv $DEST_DIR/coloquinte/README* src/coloquinte_cpp/
cp -rv $DEST_DIR/coloquinte/LIC* src/coloquinte_cpp/

echo "done"
