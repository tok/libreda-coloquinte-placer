// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! This crate is a Rust wrapper around the [Coloquinte placement algorithm](https://github.com/Coloquinte/Coloquinte_placement).

mod c_binding;

extern crate libreda_pnr;

use libreda_pnr::db as db;
use db::traits::*;
use std::collections::HashMap;
use libreda_pnr::place::placement_problem::{PlacementProblem, PlacementStatus};
use libreda_pnr::place::mixed_size_placer::{MixedSizePlacer, PlacementError};

use c_binding::*;

/// Coloquinte placement engine.
#[derive(Debug, Copy, Clone)]
pub struct Coloquinte {
    /// Height of the standard-cell row.
    row_height: i32,
}

impl<LN: db::L2NBase<Coord=i32>> MixedSizePlacer<LN> for Coloquinte {
    fn name(&self) -> &str {
        "Coloquinte"
    }

    fn find_cell_positions_impl(
        &self,
        placement_problem: &dyn PlacementProblem<LN>
    ) -> Result<HashMap<LN::CellInstId, db::SimpleTransform<LN::Coord>>, PlacementError> {


        // Convert DB to coloquinte data structures.
        let mut coloquinte_parameters = ColoquinteParameters::default();
        let cell_ids = db_to_coloquinte_parameters(&mut coloquinte_parameters, placement_problem);
        coloquinte_parameters.row_height = self.row_height;
        coloquinte_parameters.check();

        // Call placer.
        let return_value = coloquinte_place(&mut coloquinte_parameters);

        if return_value == 0 {
            // Success.

            // Convert positions.
            let positions = cell_ids.into_iter()
                .enumerate()
                .map(|(i, cell_id)| {

                    let displacement = db::Point::new(coloquinte_parameters.cell_x[i], coloquinte_parameters.cell_y[i]);
                    let position = db::SimpleTransform::translate(displacement);

                    // Take flipping into account.
                    let flip_x = coloquinte_parameters.cell_flip_x[i];
                    let flip_y = coloquinte_parameters.cell_flip_y[i];

                    let flip = if flip_x {
                        db::SimpleTransform::mirror_x()
                    } else {
                        db::SimpleTransform::identity()
                    };

                    let flip = if flip_y {
                        flip.then(&db::SimpleTransform::mirror_y())
                    } else {
                        flip
                    };

                    // Combine displacement and flipping.
                    let position_with_flip = flip.then(&position);

                    (cell_id, position_with_flip)
                })
                .collect();

            Ok(positions)

        } else {
            Err(PlacementError::Other(format!("coloquinte exited with status {}", return_value)))
        }

    }
}

/// Convert the netlist of the `top` cell into the format required by Coloquinte.
///
/// Returns a look-up table to resolve cell indices to IDs.
fn db_to_coloquinte_parameters<
    LN: db::L2NBase<Coord=i32>
>(
    params: &mut ColoquinteParameters,
    placement_problem: &dyn PlacementProblem<LN>,
) -> Vec<LN::CellInstId>
{
    let chip: &LN = placement_problem.fused_layout_netlist();
    let top: LN::CellId = placement_problem.top_cell();

    let num_cells = chip.num_child_instances(&top);

    let cell_instances = chip.each_cell_instance_vec(&top); // Resolve cell IDs by their index.
    let cell_indices: HashMap<&LN::CellInstId, usize> = cell_instances.iter()
        .enumerate()
        .map(|(a, b)| (b, a))
        .collect();

    let mut pins: Vec<LN::PinInstId> = vec![];
    let mut pin_cells = vec![];
    let mut net_limits = vec![];

    // Construct sparse form of netlist.
    {
        let nets = chip.each_internal_net(&top)
            .filter(|net| !chip.is_constant_net(net)); // Skip 0 and 1 nets.

        for net in nets {
            net_limits.push(pins.len() as i32);

            for pin_inst in chip.each_pin_instance_of_net(&net) {
                let cell_inst = chip.parent_of_pin_instance(&pin_inst);
                let cell_idx = cell_indices[&cell_inst];
                pin_cells.push(cell_idx as i32);
                pins.push(pin_inst);
            }
        }
        net_limits.push(pins.len() as i32);
    }

    let num_nets = net_limits.len() - 1;

    params.num_cells = num_cells;
    params.num_nets = num_nets;

    params.net_limits = net_limits;
    params.pin_cells = pin_cells;

    // Store cell positions.
    {
        params.pin_x_offsets = vec![0; pins.len()];
        params.pin_y_offsets = vec![0; pins.len()];

        params.cell_fixed = cell_instances.iter()
            .map(|inst| placement_problem.placement_status(inst) == PlacementStatus::Fixed)
            .collect();

        // Initial positions.
        params.cell_x = vec![0; num_cells];
        params.cell_y = vec![0; num_cells];

        for (i, cell) in cell_instances.iter().enumerate() {
            let pos = placement_problem.initial_position(cell);
            let p = pos.transform_point(db::Point::new(0, 0));
            params.cell_x[i] = p.x;
            params.cell_y[i] = p.y;
        }

        // Initial flipping.
        params.cell_flip_x = vec![false; num_cells];
        params.cell_flip_y = vec![false; num_cells];
    }

    // Store cell dimensions.
    {
        params.cell_widths.resize(num_cells, 0);
        params.cell_heights.resize(num_cells, 0);
        for (i, cell) in cell_instances.iter().enumerate() {
            let outline = placement_problem.cell_instance_outline(cell);

            if let Some(outline) = outline {
                params.cell_widths[i] = outline.width();
                params.cell_heights[i] = outline.height();
            }
        }
    }

    cell_instances
}

#[test]
fn test_convert_from_db() {
    let mut chip = db::Chip::new();
    // Create a simple netlist with 3 cells.
    let top = chip.create_cell("TOP".into());
    let mycell = chip.create_cell("MYCELL".into());
    {
        let pin1 = chip.create_pin(&mycell, "PIN1".into(), db::Direction::InOut);
        let pin2 = chip.create_pin(&mycell, "PIN2".into(), db::Direction::InOut);

        let inst1 = chip.create_cell_instance(&top, &mycell, None);
        let inst2 = chip.create_cell_instance(&top, &mycell, None);
        let inst3 = chip.create_cell_instance(&top, &mycell, None);

        let net1 = chip.create_net(&top, None);
        let net2 = chip.create_net(&top, None);

        // Create connection between cells.
        // inst1 -- inst2 -- inst3
        chip.connect_pin_instance(&chip.pin_instance(&inst1, &pin2), Some(net1.clone()));
        chip.connect_pin_instance(&chip.pin_instance(&inst2, &pin1), Some(net1.clone()));
        chip.connect_pin_instance(&chip.pin_instance(&inst2, &pin2), Some(net2.clone()));
        chip.connect_pin_instance(&chip.pin_instance(&inst3, &pin1), Some(net2.clone()));
    }

    // Define sizes of cells.
    let cell_outlines = [
        (mycell.clone(), db::Rect::new((0, 0), (10, 20)))
    ].into_iter().collect();

    let placement_problem = SimpleDesignRef {
        fused_layout_netlist: &chip,
        top_cell: top.clone(),
        cell_outlines: &cell_outlines,
        placement_region: &vec![db::Rect::new((0, 0), (1000, 1000)).into()],
        placement_status: &Default::default(), // Default to 'movable'
        net_weights: &Default::default(), // Default to 1.
        placement_location: &Default::default(),
    };

    let mut coloquinte_parameters = ColoquinteParameters::default();
    db_to_coloquinte_parameters(&mut coloquinte_parameters, &placement_problem);

    assert_eq!(coloquinte_parameters.num_cells, 3);
    assert_eq!(coloquinte_parameters.num_nets, 2);

    /// Collection of data representing the chip during the place & route flow.
    /// This struct borrows the data. In contrast, [`SimpleDesign`] owns
    /// the data.
    pub struct SimpleDesignRef<'a, C: db::L2NBase> {
        /// Base layout and netlist data-structure.
        pub fused_layout_netlist: &'a C,
        /// Cell which contains the instances to be placed.
        pub top_cell: C::CellId,
        /// Outline shapes of the cells to be placed.
        pub cell_outlines: &'a HashMap<C::CellId, db::Rect<C::Coord>>,
        /// Regions where cells are allowed to be placed.
        pub placement_region: &'a Vec<db::SimpleRPolygon<C::Coord>>,
        /// Placement status of the cell instances. Default is `Movable`.
        pub placement_status: &'a HashMap<C::CellInstId, PlacementStatus>,
        /// Net weights. Default for nets which are not in the hash map is `1.0`.
        pub net_weights: &'a HashMap<C::NetId, f64>,
        /// Overwrite the location information from the `fused_layout_netlist`.
        pub placement_location: &'a HashMap<C::CellInstId, db::SimpleTransform<C::Coord>>,
    }

    impl<'a, C: db::L2NBase> PlacementProblem<C> for SimpleDesignRef<'a, C> {
        fn fused_layout_netlist(&self) -> &C {
            self.fused_layout_netlist
        }

        fn top_cell(&self) -> C::CellId {
            self.top_cell.clone()
        }

        fn placement_region(&self) -> Vec<db::SimpleRPolygon<C::Coord>> {
            self.placement_region.to_vec()
        }

        fn initial_position(&self, cell_instance: &C::CellInstId) -> db::SimpleTransform<C::Coord> {
            self.placement_location.get(cell_instance)
                .cloned()
                .unwrap_or_else(|| self.fused_layout_netlist.get_transform(cell_instance))
        }

        fn placement_status(&self, cell_instance: &C::CellInstId) -> PlacementStatus {
            self.placement_status.get(cell_instance)
                .copied()
                .unwrap_or(PlacementStatus::Ignore)
        }

        fn cell_outline(&self, cell: &C::CellId) -> Option<db::Rect<C::Coord>> {
            self.cell_outlines.get(cell).copied()
        }

        fn net_weight(&self, net_id: &C::NetId) -> f64 {
            self.net_weights.get(net_id).copied().unwrap_or(1.0)
        }
    }
}

