// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! C bindings for coloquinte.


/// Bundle of parameters used by Coloquinte.
#[derive(Default, Clone)]
pub(crate) struct ColoquinteParameters {
    pub num_cells: usize,
    pub num_nets: usize,
    pub cell_widths: Vec<i32>,
    pub cell_heights: Vec<i32>,
    pub cell_fixed: Vec<bool>,
    pub net_limits: Vec<i32>,
    pub pin_cells: Vec<i32>,
    pub pin_x_offsets: Vec<i32>,
    pub pin_y_offsets: Vec<i32>,
    /// x-position of the cells.
    pub cell_x: Vec<i32>,
    /// y-position of the cells.
    pub cell_y: Vec<i32>,
    pub cell_flip_x: Vec<bool>,
    pub cell_flip_y: Vec<bool>,
    /// Rectangle marking the placement region.
    pub placement_region: ((i32, i32), (i32, i32)),
    /// Height of the standard-cell rows.
    pub row_height: i32,
    /// Enable detail placement after the global placement.
    pub enable_detail_placement: bool,
}

impl ColoquinteParameters {
    /// Validate parameter values for consistency.
    /// Panics if something is not consistent.
    pub fn check(&self) {
        // Validate lengths of arrays.
        assert_eq!(self.cell_widths.len(), self.num_cells);
        assert_eq!(self.cell_heights.len(), self.num_cells);
        assert_eq!(self.cell_fixed.len(), self.num_cells);
        assert_eq!(self.net_limits.len(), self.num_nets + 1);
        assert!(self.net_limits.len() > 0);

        let num_pins = self.net_limits[self.net_limits.len() - 1]
            .try_into()
            .expect("can't convert to i32");

        assert_eq!(self.pin_cells.len(), num_pins);
        assert_eq!(self.pin_x_offsets.len(), num_pins);
        assert_eq!(self.pin_y_offsets.len(), num_pins);

        assert_eq!(self.cell_x.len(), self.num_cells);
        assert_eq!(self.cell_y.len(), self.num_cells);
        assert_eq!(self.cell_flip_x.len(), self.num_cells);
        assert_eq!(self.cell_flip_y.len(), self.num_cells);

        let ((placement_region_x0, placement_region_y0), (placement_region_x1, placement_region_y1)) = self.placement_region;
        assert!(placement_region_x0 < placement_region_x1);
        assert!(placement_region_y0 < placement_region_y1);

        assert!(self.row_height > 0, "standard-cell row height must be larger than zero");
    }
}

#[test]
fn test_coloquinte_3_cells() {
    // Place 3 cells. Two have an already fixed position.

    let mut params = {
        let mut params = ColoquinteParameters::default();


        params.num_cells = 3;
        params.num_nets = 2;


        params.row_height = 20;

        params.cell_widths = vec![10; params.num_cells];
        params.cell_heights = vec![params.row_height; params.num_cells];

        params.cell_fixed = vec![true, false, true];

        params.net_limits = vec![0, 2, 4]; // Has two nets, and four pins.
        params.pin_cells = vec![
            // net1
            0, 1, // Attached to cell 0 and 1
            // net2
            1, 2  // Attached to cell 1 and 2
        ];
        params.pin_x_offsets = vec![0, 0, 0, 0];
        params.pin_y_offsets = vec![0, 0, 0, 0];

        // Cell 0 is fixed at (100, 200)
        // Cell 1 is movable
        // Cell 2 is fixed at (100, 200)
        params.cell_x = vec![100, 0, 200];
        params.cell_y = vec![200, 0, 300];


        params.cell_flip_x = vec![false; params.num_cells];
        params.cell_flip_y = vec![false; params.num_cells];

        params.placement_region = ((0, 0), (1000, 1000));

        params.enable_detail_placement = false; // Disable detail placement (would fail for yet unknown reason).
        params
    };

    params.check();

    let return_value = coloquinte_place(&mut params);

    dbg!(&params.cell_x);
    dbg!(&params.cell_y);

    assert_eq!(return_value, 0);
    assert_eq!(params.cell_x[1], 150, "cell should be placed in the center of the other two cells");
    assert_eq!(params.cell_y[1], 250);
}

/// Rust wrapper around C function of Coloquinte.
pub(crate) fn coloquinte_place(
    params: &mut ColoquinteParameters
) -> i32 {

    // Validate parameter values. Panic on failure.
    params.check();

    let ((placement_region_x0, placement_region_y0), (placement_region_x1, placement_region_y1)) = params.placement_region;

    println!("call unsafe place()");

    let return_value = unsafe {
        place(
            params.num_cells.try_into().expect("can't convert to i32"),
            params.num_nets.try_into().expect("can't convert to i32"),
            params.cell_widths.as_ptr(),
            params.cell_heights.as_ptr(),
            params.cell_fixed.as_ptr(),
            params.net_limits.as_ptr(),
            params.pin_cells.as_ptr(),
            params.pin_x_offsets.as_ptr(),
            params.pin_y_offsets.as_ptr(),
            params.cell_x.as_mut_ptr(),
            params.cell_y.as_mut_ptr(),
            params.cell_flip_x.as_mut_ptr(),
            params.cell_flip_y.as_mut_ptr(),
            placement_region_x0, placement_region_y0, placement_region_x1, placement_region_y1,
            params.row_height,
            params.enable_detail_placement,
        )
    };


    println!("done unsafe place()");

    return_value
}

#[link(name = "coloquinte_wrapper", kind = "static")]
extern "C" {
    /// Binding to C function of Coloquinte.
    fn place(
        nb_cells: i32,
        nb_nets: i32,
        cell_widths: *const i32,
        cell_heights: *const i32,
        cell_fixed: *const bool,
        net_limits: *const i32,
        pin_cells: *const i32,
        pin_x_offsets: *const i32,
        pin_y_offsets: *const i32,
        cell_x: *mut i32,
        cell_y: *mut i32,
        cell_flip_x: *mut bool,
        cell_flip_y: *mut bool,
        placement_region_x0: i32,
        placement_region_y0: i32,
        placement_region_x1: i32,
        placement_region_y1: i32,
        row_height: i32,
        enable_detail_placement: bool,
    ) -> i32;
}
