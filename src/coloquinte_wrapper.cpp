// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
// SPDX-FileCopyrightText: 2022 Gabriel Gouvine
//
// SPDX-License-Identifier: AGPL-3.0-or-later


/*
 * This file provides a one-function API for the Coloquinte placement algorithm.
 * The exported C function `place` is intended to be used from the LibrEDA framework.
 */

#include <iostream>

#include "coloquinte/circuit.hxx"
#include "coloquinte/legalizer.hxx"

#include <iostream>
#include <vector>
#include <ctime>

using namespace coloquinte::gp;
using namespace coloquinte;


void output_stdout(netlist const & circuit, placement_t const & pl, box<int_t> surface){
    std::cout << surface.x_min_ << " " << surface.x_max_ << std::endl;
    std::cout << surface.y_min_ << " " << surface.y_max_ << std::endl;
    std::cout << std::endl;
    for(index_t i=0; i<circuit.cell_cnt(); ++i){
        std::cout << pl.positions_[i].x_ << " " << pl.positions_[i].y_ << std::endl;
    }
}

void output_report(netlist const & circuit, placement_t const & LB_pl, placement_t const & UB_pl){
    std::cout << "HPWL: " << get_HPWL_wirelength(circuit, UB_pl);
    std::cout << "\tRSMT: " << get_RSMT_wirelength(circuit, UB_pl);
    //std::cout << "\tMST: " << get_MST_wirelength(circuit, UB_pl);
    std::cout << "\tTime: " << time(NULL) << "\tLinear D: " << get_mean_linear_disruption(circuit, LB_pl, UB_pl) << "\tQuad D: " << get_mean_quadratic_disruption(circuit, LB_pl, UB_pl) << std::endl;
    //std::cout << "HPWL: " << get_HPWL_wirelength(circuit, UB_pl) << "\tTime: " << time(NULL) << "\tLinear D: " << get_mean_linear_disruption(circuit, LB_pl, UB_pl) << "\tQuad D: " << get_mean_quadratic_disruption(circuit, LB_pl, UB_pl) << std::endl;
}
void output_report(netlist const & circuit, placement_t const & LB_pl){
    std::cout << "HPWL: " << get_HPWL_wirelength(circuit, LB_pl);
    std::cout << "\tRSMT: " << get_RSMT_wirelength(circuit, LB_pl);
    //std::cout << "\tMST: " << get_MST_wirelength(circuit, LB_pl);
    std::cout << "\tTime: " << time(NULL) << std::endl;
    //std::cout << "HPWL: " << get_HPWL_wirelength(circuit, LB_pl) << "\tTime: " << time(NULL) << std::endl;
}

/** C interface used by the Rust crate.
*/
extern "C" int place(
        int nb_cells, // Number of cells.
        int nb_nets, // Number of nets.
        int *cell_widths,
        int *cell_heights,
        char *cell_fixed,
        int *net_limits,
        int *pin_cells,
        int *pin_x_offsets,
        int *pin_y_offsets,
        int *cell_x,
        int *cell_y,
        char *cell_flip_x,
        char *cell_flip_y,
        int x_min, // Lower left corner of placement region.
        int y_min,
        int x_max, // Upper right corner of placement region.
        int y_max,
        int row_height,
        bool enable_detail_placement
        ) {

    std::cout << "coloquinte" << std::endl;
    std::cout << "num cells: " << nb_cells << std::endl;
    std::cout << "num nets: " << nb_nets << std::endl;

    index_t cell_cnt = nb_cells;
    index_t net_cnt = nb_nets;

    // Allocate buffers for constructing netlist.
    std::vector<temporary_cell> cells(cell_cnt);
    std::vector<point<int_t>> positions(cell_cnt);
    std::vector<point<bool>> orientations(cell_cnt);
    std::vector<temporary_net> nets(net_cnt);
    std::vector<temporary_pin> pins;

    // Populate the netlist.
    try {
        // Create cells.
        for(size_t i = 0; i < cell_cnt; ++i) {
            int x_s = cell_widths[i];
            int y_s = cell_heights[i];

            cells[i].size = point<int_t>(x_s, y_s);
            cells[i].list_index = i;
            cells[i].area = static_cast<capacity_t>(x_s) * static_cast<capacity_t>(y_s);
        }

        // Create nets and pins.
        // Loop over nets.
        for(size_t i = 0; i < net_cnt; ++i) {
            size_t pin_start_idx = net_limits[i];
            size_t pin_end_idx = net_limits[i+1];

            nets[i] = temporary_net(i, 1);

            // Loop over pins.
            for(size_t pin_idx = pin_start_idx; pin_idx < pin_end_idx; ++pin_idx) {

                // Get pin offset.
                coloquinte::float_t pin_off_x = static_cast<coloquinte::float_t>(pin_x_offsets[pin_idx]);
                coloquinte::float_t pin_off_y = static_cast<coloquinte::float_t>(pin_y_offsets[pin_idx]);



                // The pin's information is only given in the temporary_pin structure: nets and cells create pointers to the pins during the construction

                // Get the cell where this pin is attached.
                index_t cell_ind = pin_cells[pin_idx];

                // Get cell position.
                // coloquinte::float_t x = static_cast<coloquinte::float_t>(cell_x[pin_idx]);
                // coloquinte::float_t y = static_cast<coloquinte::float_t>(cell_y[pin_idx]);

                // Put the pin at the cell center.
                // Compute position of cell center.
                point<coloquinte::float_t> pin_offset(pin_off_x, pin_off_y);
                point<coloquinte::float_t> cell_size(cells[cell_ind].size);
                point<coloquinte::float_t> pin_location = pin_offset + 0.5f * cell_size;

                pins.push_back(
                    temporary_pin(
                        pin_location,
                        cell_ind,
                        i
                    )
                );

            }

        }


        // Set cell positions.
        for(size_t i = 0; i < cell_cnt; ++i) {
            int x = cell_x[i];
            int y = cell_y[i];
            positions[i] = point<int_t>(x, y);

            bool flip_x = cell_flip_x[i];
            bool flip_y = cell_flip_y[i];

            orientations[i] = point<bool>(flip_x, flip_y);
            orientations[i] = point<bool>(flip_x, flip_y);

            bool fixed = cell_fixed[i];
            if(fixed){
                cells[i].attributes = 0;
            } else{
                cells[i].attributes = XMovable | YMovable | XFlippable | YFlippable;
            }

        }
    }  catch (const std::exception& e) {

      std::cout << "initialization terminated with exception: " << e.what() << std::endl;
      return -1;

    }

    // Rectangular placement region.
    box<int_t> surface = box<int_t>(x_min, x_max, y_min, y_max);

    // Assemble netlist.
    netlist circuit = netlist(cells, nets, pins);
    circuit.selfcheck();

    // Initial placement.
    placement_t placement;
    placement.positions_ = positions;
    placement.orientations_ = orientations;
    placement.selfcheck();

    placement_t LB_pl = placement;
    placement_t UB_pl = LB_pl;

    // Place the netlist.
    try {
        std::cout << "The initial wirelength is " << get_HPWL_wirelength(circuit, LB_pl) << " at " << time(NULL) << std::endl;

        auto first_legalizer = get_rough_legalizer(circuit, LB_pl, surface);
        first_legalizer.selfcheck();
        get_rough_legalization(circuit, UB_pl, first_legalizer);
        UB_pl.selfcheck();
        std::cout << "The simply legalized wirelength is " << get_HPWL_wirelength(circuit, UB_pl) << " at " << time(NULL) << " with linear disruption " << get_mean_linear_disruption(circuit, LB_pl, UB_pl) << " and quadratic disruption " << get_mean_quadratic_disruption(circuit, LB_pl, UB_pl) << std::endl;
        LB_pl = UB_pl;

        // Early topology-independent solution
        auto solv = get_star_linear_system(circuit, LB_pl, 1.0, 0, 10000)
            + get_pulling_forces(circuit, UB_pl, 1000000.0); // Big distance: doesn't pull strongly, but avoids problems if there are no fixed pins
        std::cout << "Star optimization at time " << time(NULL) << std::endl;
        solve_linear_system(circuit, LB_pl, solv, 200); // number of iterations=200
        output_report(circuit, LB_pl);

        coloquinte::float_t pulling_force = 0.01;
        for(int i=0; i<1; ++i, pulling_force += 0.03){
            // Create a legalizer and bipartition it until we have sufficient precision (~2 to 10 standard cell widths)
            auto legalizer = get_rough_legalizer(circuit, LB_pl, surface);
            for(int quad_part =0; 10u * (1u << (2*quad_part)) < circuit.cell_cnt(); quad_part++){ // Here, approximately 10 cells in each region
                legalizer.x_bipartition();
                legalizer.y_bipartition();
                legalizer.redo_diagonal_bipartitions();
                legalizer.redo_line_partitions();
                legalizer.redo_diagonal_bipartitions();
                legalizer.redo_line_partitions();
                legalizer.redo_diagonal_bipartitions();
                legalizer.selfcheck();
            }
            UB_pl = LB_pl; // Keep the orientation between LB and UB

            get_rough_legalization(circuit, UB_pl, legalizer);
            std::cout << "Roughly legalized" << std::endl;
            output_report(circuit, LB_pl, UB_pl);

            auto LEG = dp::legalize(circuit, UB_pl, surface, row_height);
            dp::get_result(circuit, LEG, UB_pl);
            std::cout << "Legalized" << std::endl;
            output_report(circuit, LB_pl, UB_pl);

            // Get the system to optimize (tolerance, maximum and minimum pin counts) and the pulling forces (threshold distance)
            auto solv = get_HPWLF_linear_system(circuit, LB_pl, 0.01, 2, 100000)
                + get_linear_pulling_forces(circuit, UB_pl, LB_pl, pulling_force, 40.0);
            std::cout << "Got the linear system at time " << time(NULL) << std::endl;
            solve_linear_system(circuit, LB_pl, solv, 400); // number of iterations
            output_report(circuit, LB_pl);

            // Optimize orientation sometimes
            if(i%5 == 0){
                optimize_exact_orientations(circuit, LB_pl);
                std::cout << "Oriented" << std::endl;
                output_report(circuit, LB_pl);
            }
        }


        placement_t final_placement;


        if(enable_detail_placement) {
            std::cout << "Now let's detailed place" << std::endl;
            for(index_t i=0; i<2; ++i){
                optimize_exact_orientations(circuit, UB_pl);
                std::cout << "Oriented" << std::endl;
                output_report(circuit, LB_pl, UB_pl);

                auto LEG = dp::legalize(circuit, UB_pl, surface, 12);
                dp::get_result(circuit, LEG, UB_pl);
                std::cout << "Legalized" << std::endl;
                output_report(circuit, LB_pl, UB_pl);

                //dp::swaps_global_HPWL(circuit, LEG, 3, 4);
                //dp::get_result(circuit, LEG, UB_pl);
                //std::cout << "Global swaps" << std::endl;
                //output_report(circuit, LB_pl, UB_pl);

                dp::OSRP_convex_HPWL(circuit, LEG);
                dp::get_result(circuit, LEG, UB_pl);
                std::cout << "Ordered row optimization" << std::endl;
                output_report(circuit, LB_pl, UB_pl);

                dp::swaps_row_convex_HPWL(circuit, LEG, 4);
                dp::get_result(circuit, LEG, UB_pl);
                std::cout << "Local swaps" << std::endl;
                output_report(circuit, LB_pl, UB_pl);
            }

            final_placement = UB_pl;
        } else {
            // Skip detail placement.
            final_placement = LB_pl;
        }


        // Write back cell locations.
        for(size_t i = 0; i < cell_cnt; ++i) {
            cell_x[i] = final_placement.positions_[i].x_;
            cell_y[i] = final_placement.positions_[i].y_;

            cell_flip_x[i] = final_placement.orientations_[i].x_;
            cell_flip_y[i] = final_placement.orientations_[i].y_;
        }

        return 0;

    } catch (const std::exception& e) {

        std::cout << "placement terminated with exception: " << e.what() << std::endl;
        return -1;

    }


}

