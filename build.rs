// SPDX-FileCopyrightText: 2022 Thomas Kramer
//
// SPDX-License-Identifier: CC0-1.0

use std::fs;
use std::ffi::OsStr;
use std::path::Path;

fn main() {

    let coloquinte_home = Path::new("src/coloquinte_cpp");
    let coloquinte_src = coloquinte_home.join("src");

    // List all .cxx files
    let dir_content = fs::read_dir(&coloquinte_src).unwrap();

    let extension = OsStr::new("cxx");

    let cxx_files = dir_content.into_iter()
        .filter_map(|entry| entry.ok())
        .filter(|entry| entry.path().extension() == Some(extension))
        .filter(|entry| entry.file_name() != "main.cxx") // Ignore the main program of coloquinte.
        .map(|entry| entry.path());

    cc::Build::new()
        .cpp(true)
        .include(coloquinte_src.join("coloquinte"))
        .include(coloquinte_src)
        .files(cxx_files)
        .file("src/coloquinte_wrapper.cpp")
        //.flag_if_supported("-fopenmp") // Enable OpenMP for parallelization.
        .flag_if_supported("-Wno-unknown-pragmas") // Suppress warning about unknown OpenMP pragmas (happens when -fopenmp is not set).
        .compile("coloquinte_wrapper");
}
